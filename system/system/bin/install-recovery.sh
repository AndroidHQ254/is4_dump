#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:103529291c2b8f7e86f887eb47e5b6a1e8b50737; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:287315d1df2a8e64637c8507ea64fa28793bc25f EMMC:/dev/block/platform/bootdevice/by-name/recovery 103529291c2b8f7e86f887eb47e5b6a1e8b50737 33554432 287315d1df2a8e64637c8507ea64fa28793bc25f:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
